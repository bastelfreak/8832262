#!/bin/bash
get_html_title (){
  curl --silent "${1}" | awk -vRS="</title>" '/<title>/{gsub(/.*<title>|\n+/,"");print;exit}'
}
get_html_title $1